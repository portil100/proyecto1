import { Tab1Page } from './../../tab1/tab1.page';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../servicios/auth.service';
import { Router } from '@angular/router';
import { EmailValidator } from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string
  password: string;

  passwordType: string = 'password';
  passwordShown: boolean = false;


  constructor(private authService: AuthService, public router: Router) { }

  ngOnInit() {
  }

  /**
   * Metodo para ver u ocultar la contraseña
   */
  public tooglePassword() {
    if (this.passwordShown) {
      this.passwordShown = false;
      this.passwordType = 'password';
    } else {
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }

  /**
   * Metodo para iniciar sesion
   */
  onSubmitLogin() {
    this.authService.login(this.email, this.password).then(res => {
      this.router.navigateByUrl('landing');
      this.authService.showmessage('Ha iniciado sesion correctamente');
    }).catch(err => this.authService.showmessage('Los datos son incorrectos o no existe el usuario'));
  }

  /**
   * Metodo para ir hacia la pagina de registro
   */
  onSubmitRegister() {
    this.router.navigateByUrl('registro');

  }

  /**
   * Metodo para iniciar sesion a traves de Google
   */
  onLoginGoogle() {
    this.authService.loginGoogle()
      .then((res) => {
        this.router.navigateByUrl('landing');
        this.authService.showmessage('Ha iniciado sesion correctamente');
      }).catch(err => this.authService.showmessage('Los datos son incorrectos o no existe el usuario'))
  }

  /**
   * Llama al metodo de resetear la contraseña
   */
  onResetPassword() {
    this.authService.resetPassword(this.email);

  }


}
