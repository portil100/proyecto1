import { UsuariosService } from './../../servicios/usuarios.service';
import { TorneoService } from './../../servicios/torneo.service';
import { Component, OnInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth.service';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],

})

export class LandingPage implements OnInit {

  public torneoImg: any = [];

  public tab = 0;


  @ViewChild('password') password: ElementRef;

  public user = {
    name: '',
    surname: '',
    date: new Date(),
    phone: 0,
    direction: '',
    postal: 0,
    email: ''
  }

  /**
   * Para obtener el dia actual
   */
  public diaReserva = new Date().toISOString().slice(0, 10);

  public reservas = [];
  public reservasByDate = [[]];
  public reservaPistas = [
    {
      date: '',
      email: '',
      hora: -1,
    },
    {
      date: '',
      email: '',
      hora: -1,
    },
    {
      date: '',
      email: '',
      hora: -1,
    },
    {
      date: '',
      email: '',
      hora: -1,
    },
    {
      date: '',
      email: '',
      hora: -1,
    }
  ];

  /**
   * Le doy formato a los valores cuando vaya a mostrarlos por pantalla
   */
  nombrepista = ['Pista 1', 'Pista 2', 'Pista 3', 'Pista 4', 'Pista 5'];
  horas = ['9:00-10:30', '10:30-12:00', '12:00-13:30'];

  selectValue = [0, 1, 2]; //Para poner a int los valores del select

  constructor(private authService: AuthService, public renderer: Renderer2,
    public actionSheetController: ActionSheetController, public torneoservice: TorneoService,
    public userservice: UsuariosService, public db: UsuariosService) { }



  ngOnInit() {
    this.torneoservice.getTorneo().subscribe(torneos => {
      this.torneoImg = torneos;
    });
  }

  /**
   * Con esto activo los eventos cada vez que ingreso a la vista
   */
  ionViewWillEnter() {
    let email = this.authService.getAuthEmail();
    this.db.getActiveUser(email).then((res: any) => {
      this.user = res;
      this.getReservas();
      this.getReservasByDate(this.diaReserva);
      console.log(res);
    }).catch(err => {
      console.error(err);
    })
  }

/**
 * Actualiza las pistas cada vez que cambio la vista
 * @param tab 
 */
  setTab(tab: number) {
    switch (tab) {
      case 1:
        this.getReservas()
        this.getReservasByDate(this.diaReserva);
        break;
      default:
        break;
    }
    this.tab = tab;
  }

  /**
   * Cierre de Sesion
   */
  Onlogout() {
    this.authService.logout();
    this.authService.showmessage('Has cerrado sesion');
  }

  /**
   * Bara Lateral con el logout
   */
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Opciones',
      buttons: [{
        text: 'Cerrar Sesión',
        role: 'destructive',
        icon: 'log-out',
        handler: () => {
          this.Onlogout();
        },
      }]
    });
    await actionSheet.present();
  }

  /**
   * Metodo para actualizar la BD con los datos del cliente
   */
  public saveUserData() {
    console.log(this.user);
    this.db.updateUserData(this.user);
    this.authService.showmessage("Se han subido los datos correctamente");
  }

  resetPistas() {
    this.reservaPistas = [
      {
        date: '',
        email: '',
        hora: -1,
      },
      {
        date: '',
        email: '',
        hora: -1,
      },
      {
        date: '',
        email: '',
        hora: -1,
      },
      {
        date: '',
        email: '',
        hora: -1,
      },
      {
        date: '',
        email: '',
        hora: -1,
      }
    ];
  }

  /**
   * Actualiza las lista de las reserva dependiendo del dia
   * 
   */
  public updateRoster(event) {
    console.log(event);
    this.diaReserva = event;
    this.getReservasByDate(this.diaReserva);
    this.resetPistas();
  }

  /**
   * Devuelve las reservas del usuario activo.
   * @returns lista_reservas
   */
  public getReservas() {
    this.db.getReservas(this.user.email)
      .then((res: any) => {
        this.reservas = res;
        console.log('mis reservas', this.reservas);
      }).catch(err => {
        console.error(err);
      })
  }

  /**
   * Devuelve las reservas por fecha
   * @returns lista_reservas
   */
  public getReservasByDate(date: string) {
    this.db.getReservasByDate(date)
      .then((res: any) => {
        this.reservasByDate = res;
        console.log(this.reservasByDate);
      }).catch(err => {
        console.error(err);
      })
  }

  /**
   * Sube las reservas a la BD
   */
  public guardarReserva() {
    for (let i = 0; i < this.reservaPistas.length; i++) {
      this.reservaPistas[i].date = this.diaReserva;
      this.reservaPistas[i].email = this.user.email;
    }
    console.log(this.reservaPistas)
    this.db.guardarReservas(this.reservaPistas);
    this.getReservas();
    this.authService.showmessage("Se ha reservado la pista correctamente");
  }

  /**
   * Metodo para borrar las pistas 
   * @param reser 
   * @return lista_reservas
   */
  public cancelPistas(reser) {
    console.log('page: ' + reser);
    this.db.borrarReservas(reser);
    this.authService.showmessage("Se ha cancelado su pista con exito")
    this.getReservas();
  }
}


