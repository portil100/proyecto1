import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../servicios/auth.service';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  public email: string;
  public name: string;
  public password: string;

  passwordType: string = 'password';
  passwordShown: boolean = false;

  constructor(public router: Router, private auth: AuthService) { }

  ngOnInit() {

  }

  /**
   * Metodo para ver u ocultar la contraseña
   */
  public tooglePassword() {
    if (this.passwordShown) {
      this.passwordShown = false;
      this.passwordType = 'password';
    } else {
      this.passwordShown = true;
      this.passwordType = 'text';
    }
  }

  /**
   * Metodo para registrar a los usuario a traves de la contraseña, nombre y corre
   */
  OnSubmitRegister() {
    this.auth.register(this.email, this.password, this.name).then(auth => {
      this.auth.showmessage('Se ha registrado correctamente');
    }).catch(err => this.auth.showmessage('Ha habido un error con el registro, hay un problema con el email o la contraseña no tiene mas de 6 caracteres'));
  }

  /**
   * Navegar al la pagina del login
   */
  login() {
    this.router.navigateByUrl('login');

  }

}
