import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})

export class UsuariosService {

  constructor(private db: AngularFirestore) { }


  /**
   * Me devuelve los datos del usuario cuyo email coindice con el que se ha logeado
   */
  getActiveUser(email: string) {
    return new Promise((resolve) => {
      this.db.collection('users', ref => ref.where('email', '==', email)).get().toPromise()
        .then(res => {
          res.forEach((item) => {
            resolve(item.data());
          })
        }).catch(err => {
          resolve(undefined)
          console.error(err)
        })
    });
  }

  /**
   * Metodo que actualiza los datos de los usuarios
   * @param user 
   */
  updateUserData(user: any) {
    this.db.collection('users', ref => ref.where('email', '==', user.email)).get().toPromise()
      .then(res => {
        res.forEach((item) => {
          item.ref.update(user)
            .then(res => console.log(res))
            .catch(err => console.error(err));
        })
      }).catch(err => {
        console.log(err);
      })
  }

  /**
   * Me devuelve las reservas por email
   * @param email 
   */
  getReservas(email: string) {
    return new Promise((resolve) => {
      this.db.collection('reservas', ref => ref.where('email', '==', email)).get().toPromise()
        .then(res => {
          let reservas = [];
          res.forEach(item => {
            let data = item.data();
            data.id = item.id;
            reservas.push(data);
          })
          resolve(reservas);
        }).catch(err => {
          console.error(err);
        })
    })
  }

  /**
   *  Me devuelve las reservas por la fecha
   * @param date 
   */
  getReservasByDate(date: string) {
    return new Promise((resolve) => {
      this.db.collection('reservas', ref => ref.where('date', '==', date)).get().toPromise()
        .then(res => {
          let reservas = [[], [], [], [], []];
          res.forEach(item => {
            reservas[item.data().npista].push(item.data().hora);
          })
          resolve(reservas);
        }).catch(err => {
          console.error(err);
        })
    })
  }

  /**
   * Metodo que me guarda las reservas de las pistas en la BD
   * @param reservas 
   */
  guardarReservas(reservas: any[]) {
    for (let i = 0; i < reservas.length; i++) {
      if (reservas[i].hora != -1) {
        this.db.collection('reservas').add({ npista: i, date: reservas[i].date, email: reservas[i].email, hora: reservas[i].hora })
          .catch(err => {
            console.error(err);
          })
      }
    }
  }

  /**
   * Metodo para borrar las reservas de la base de datos
   * @param reserva
   */
  borrarReservas(reserva: any) {
    console.log('db: ' + reserva);
    this.db.collection('reservas').doc(reserva.id).delete()
    .catch(err => {
      console.error(err);
    });
  }
}

