import { Injectable } from '@angular/core';
import { AngularFirestore } from "@angular/fire/firestore";
import { map } from "rxjs/operators";

export interface torneo {
  id: string
  imagen: string
}

@Injectable({
  providedIn: 'root'
})
export class TorneoService {

  constructor(private db: AngularFirestore) { }

  
  getTorneo(){
    return this.db.collection('torneos').snapshotChanges().pipe(map(image => {
      return image.map(a =>{
        const data = a.payload.doc.data() as torneo;
        data.id = a.payload.doc.id;
        return data;
      })
    }))
  }
}
