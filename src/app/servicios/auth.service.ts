import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { rejects } from 'assert';
import { reject } from 'q';
import { ToastController } from '@ionic/angular';
import { auth } from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private email;

  constructor(private AFauth: AngularFireAuth, private toastctrl: ToastController, private router: Router, private db: AngularFirestore) { }

  /**
   * Metodo de inicio de sesion 
   * @param email 
   * @param password 
   * @returns email
   * @returns password
   */
  login(email: string, password: string) {
    return new Promise((resolve, rejected) => {
      this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
        this.email = email;
        resolve(user);
      }).catch(err => rejected(err));
    });
  }

  /**
   * Me devuelve el email del usuario logeado
   * @returns email
   */
  getAuthEmail() {
    return this.email;
  }



  /**
   * Login con Google
   */
  loginGoogle() {
    return this.AFauth.auth.signInWithPopup(new auth.GoogleAuthProvider());
  }

  /**
   * Metodo para registrarnos
   * @param email 
   * @param password 
   * @param name 
   */
  register(email: string, password: string, name: string) {
    return new Promise((resolve, reject) => {
      this.AFauth.auth.createUserWithEmailAndPassword(email, password).then(res => {
        const uid = res.user.uid;
        this.db.collection('users').doc(res.user.uid).set({
          name: name,
          email: email,
          password: password,
          uid: uid
        })
        resolve(res)
      }).catch(err => reject(err))
    })
  }

  /**
   * Toast que muestra un mensaje (2 segundos)
   */
  async showmessage(message: string) {
    let toast = await this.toastctrl.create({
      duration: 2000,
      message: message,
      position: 'bottom'
    });
    await toast.present();

  }

  /**
   * Cierre de sesion
   */
  logout() {
    this.AFauth.auth.signOut().then(() => {
      this.router.navigateByUrl('login');
    });
  }

  /**
   * Metodo para resetear la contraseña
   * @param email 
   */
  resetPassword(email: string) {
    this.AFauth.auth.sendPasswordResetEmail(email).then(function () {
    }).catch(function (error) {
    });
  }

}
